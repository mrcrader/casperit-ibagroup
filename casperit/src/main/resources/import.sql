insert into employee (id, name, surname, position, phone, email) values (1, 'Дмитрий', 'Ахремчик', 'Руководитель проекта', '1234', 'ahremchik@example.com');

insert into meetingroom (id, CommonNameResourceName, Site, ResourceDescription, Capacity, ResourceType, AutoProcessType, autoProcessForwardTo_id, alertTo_id) values (1, 'ул. Программистов 5/ 514', 'ул. Программистов 5', 'Переговорная комната для общения с заказчиками. Оборудованна доской и маркерами.', '20', '1', '2', 1, 1);

insert into reservation (id, EndTime, OptionalDescription, Phone, ReservationDescription, StartTime, meetingroom_id, reservedBy_id) values (1, '2019-04-25', 'Нужно срочно!', '1234', 'Ну очень срочно нужна переговорка!', '2019-04-25', 1, 1);
