package com.ibagroup.casperit.reservation.database.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ibagroup.casperit.meetingroom.database.dto.Meetingroom;


/**
 * 
 * @author IBA Group
 * @since 2019
 *
 */

@SuppressWarnings("serial")
@Entity
@Table(name="Reservation")
public class Reservation implements Serializable {

	/**
	 * <p> ID </p>
	 */
	@Id
    @GeneratedValue
    private Long id;
	
	/**
	 * <p> Description about meeting room. </p>
	 */
	@Column(name="ReservationDescription")
	private String reservationDescription;
	
	/**
	 * <p> Phone number (mobile). </p>
	 */
	@Column(name="Phone")
	private String phone;
	
	/**
     * <p>
     * Start time of the meeting.
     * </p>
     * 
     */
	@Column(name="StartTime")
	private Long startTime;
	
	/**
     * <p>
     * Start date of the meeting.
     * </p>
     * 
     */
	@Column(name="StartDate")
	private Long startDate;
	
    /**
     * <p>
     * Start time of the meeting end.
     * </p>
     */
	@Column(name="EndTime")
	private Long endTime;
	
	 /**
     * <p>
     * Start date of the meeting end.
     * </p>
     */
	@Column(name="EndDate")
	private Long endDate;
	
	/**
	 * <p> Who has reserved this room. </p>
	 */
	@Column(name="ReservedBy")
	private String reservedBy;
	
	/**
	 * <p> Data about meeting room. </p>
	 */
	@ManyToOne
	private Meetingroom meetingroom;
	
	/**
	 * <p> Optional comments. </p>
	 */
	@Column(name = "OptionalDescription")
	private String optionalDescription;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReservationDescription() {
		return reservationDescription;
	}

	public void setReservationDescription(String reservationDescription) {
		this.reservationDescription = reservationDescription;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public String getReservedBy() {
		return reservedBy;
	}

	public void setReservedBy(String reservedBy) {
		this.reservedBy = reservedBy;
	}

	public Meetingroom getMeetingroom() {
		return meetingroom;
	}

	public void setMeetingroom(Meetingroom meetingroom) {
		this.meetingroom = meetingroom;
	}

	public String getOptionalDescription() {
		return optionalDescription;
	}

	public void setOptionalDescription(String optionalDescription) {
		this.optionalDescription = optionalDescription;
	}
	
}
