package com.ibagroup.casperit.reservation.web.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ibagroup.casperit.meetingroom.web.mapper.MeetingroomBeanMapper;
import com.ibagroup.casperit.reservation.database.dto.Reservation;
import com.ibagroup.casperit.reservation.web.bean.ReservationBean;

public class ReservationBeanMapper {
	
	private MeetingroomBeanMapper meetingroomMapper = new MeetingroomBeanMapper();
	
	public ReservationBean toBean(Reservation dto) {
		ReservationBean bean = null;
		if (dto != null) {
			bean = new ReservationBean();
			bean.setId(dto.getId());
			bean.setReservationDescription(dto.getReservationDescription());
			bean.setEndTime(dto.getEndTime());
			bean.setEndDate(dto.getEndDate());
			bean.setOptionalDescription(dto.getOptionalDescription());
			bean.setPhone(dto.getPhone());
			bean.setStartTime(dto.getStartTime());
			bean.setStartDate(dto.getStartDate());
			bean.setMeetingroom(meetingroomMapper.toBean(dto.getMeetingroom()));
			bean.setReservedBy(dto.getReservedBy());
		}
		return bean;
		
	}

	public List<ReservationBean> toBeanList(List<Reservation> reservationEntities){
		final List<ReservationBean> reservationBeans = new ArrayList<ReservationBean>();
		if(reservationEntities != null) {
			for(Reservation reservationEntity : reservationEntities) {
				ReservationBean bean = new ReservationBean();
				bean.setId(reservationEntity.getId());
				bean.setReservationDescription(reservationEntity.getReservationDescription());
				bean.setEndTime(reservationEntity.getEndTime());
				bean.setEndDate(reservationEntity.getEndDate());
				bean.setOptionalDescription(reservationEntity.getOptionalDescription());
				bean.setPhone(reservationEntity.getPhone());
				bean.setStartTime(reservationEntity.getStartTime());
				bean.setStartDate(reservationEntity.getStartDate());
				bean.setMeetingroom(meetingroomMapper.toBean(reservationEntity.getMeetingroom()));
				bean.setReservedBy(reservationEntity.getReservedBy());
				reservationBeans.add(bean);
			}
		}
		return reservationBeans;
		
	}
	
}
