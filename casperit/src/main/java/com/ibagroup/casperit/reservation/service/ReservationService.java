package com.ibagroup.casperit.reservation.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.PathParam;

import com.ibagroup.casperit.reservation.database.dao.ReservationDao;
import com.ibagroup.casperit.reservation.database.dao.ReservationSearchParams;
import com.ibagroup.casperit.reservation.database.dto.Reservation;


/**
 * 
 * @author IBA Group
 * @since 2019
 * 
 * A service for {@link Reservation}
 *
 */

@Stateless
public class ReservationService {
	
	@Inject
	private ReservationDao dao;
	
	static final long ONE_MINUTE_IN_MILLIS = 60000; //milliseconds
	
	/*
	 * Method for invoke all reservations by params
	 */
	public List<Reservation> retrieveReservation(ReservationSearchParams params){
		try {
			return dao.retrieveReservationByParams(params);
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<Reservation> retrieveReservationByMeetingroomIdAndTimeParams(ReservationSearchParams params){
		try {
			return dao.retrieveReservationByMeetingroomIdAndTimeParams(params);
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<Reservation> retrieveReservationByEmployeeId(ReservationSearchParams params){
		try {
			return dao.retrieveReservationByEmployeeId(params);
		} catch (Exception e) {
			return null;
		}
	}
    
    public void deleteReservationById(Long id) {
    	try {
    		dao.deleteReservationById(id);
    	} catch (Exception e) {
    		
		}
    }
    
    public void createNewReservation(Reservation dto) throws Exception {
    	try {
    		dao.createNewReservation(dto);
    	} catch (Exception e) {
  
		}
    
    }
    
    public void updateReservationById(Long id, Reservation dto) {
    	try {
    		dao.updateReservationById(id, dto);
    	} catch (Exception e) {

		}
    }
    
    public List<Reservation> retrieveReservationsByStartTime(Long startTime) {
    	try {
    		return dao.retrieveReservationsByStartTime(startTime);
    	} catch (Exception e) {
			return null;
		}
    }

    public void createNewReservationAndCheckAvailability(Reservation dto) throws Exception {
//    	SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//    	
//    	Date userStartTime = dto.getStartTime(); 
//    	String str1 = dateformat.format(userStartTime);
//    	System.out.println(str1);
//    	
//    	Date userEndTime = dto.getEndTime();
//    	String str2 = dateformat.format(userEndTime);
//    	System.out.println(str2);
//    	
//    	Date start = dateformat.parse(str1);
//    	Date end = dateformat.parse(str2);
//    	
//    	if (dao.checkMeetingRoomAvailabilityByTimeParams(start, end).isEmpty()) {
    	try{
    		dao.createNewReservation(dto);
    	} catch (Exception e) {
			// TODO: handle exception
		}
//    	
//    	} else {
//    			System.out.println("Reservaed!");
//    	}
    }
    
    public Boolean isMeetingroomEmpty(@PathParam("meetingroomId") Long meetingroomId) throws ParseException{
		
		ReservationSearchParams paramsReservation = new ReservationSearchParams();
		
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		
		Date currentTimePlusTenMinutesUnparse = new Date(System.currentTimeMillis() - (10 * ONE_MINUTE_IN_MILLIS));
		String str1 = dateformat.format(currentTimePlusTenMinutesUnparse);
		Date currentTimePlusTenMinutes = dateformat.parse(str1);
		
		Date currentTimeMinusTenMinutesUnparse = new Date(System.currentTimeMillis() - (10 * ONE_MINUTE_IN_MILLIS));
		String str2 = dateformat.format(currentTimeMinusTenMinutesUnparse);
		Date currentTimeMinusTenMinutes = dateformat.parse(str2);
		
		Date currentTimeUnparse = new Date(System.currentTimeMillis());
		String str3 = dateformat.format(currentTimeUnparse);
		Date currentTime = dateformat.parse(str3);
		
		paramsReservation.setMeetingroomId(meetingroomId);
		
		paramsReservation.setCurrentTimeMinusTenMinutes(currentTimeMinusTenMinutes);
		paramsReservation.setCurrentTimePlusTenMinutes(currentTimePlusTenMinutes);
		paramsReservation.setCurrentTime(currentTime);
		
		try {
			if(retrieveReservationByMeetingroomIdAndTimeParams(paramsReservation).isEmpty()) {
				return true;
			} else {
					return false;
				   }
		} catch (Exception e) {
			return null;
		}
		
	}
    
}
