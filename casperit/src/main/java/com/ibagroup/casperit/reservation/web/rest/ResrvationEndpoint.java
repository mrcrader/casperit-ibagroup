package com.ibagroup.casperit.reservation.web.rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.ibagroup.casperit.reservation.database.dao.ReservationSearchParams;
import com.ibagroup.casperit.reservation.database.dto.Reservation;
import com.ibagroup.casperit.reservation.service.ReservationService;
import com.ibagroup.casperit.reservation.web.bean.ReservationBean;
import com.ibagroup.casperit.reservation.web.mapper.ReservationBeanMapper;
import com.ibagroup.casperit.reservation.web.mapper.ReservationDtoMapper;

@Path("reservation")
@Stateless
public class ResrvationEndpoint {

	/**
	 * <p>
	 * {@link ReservationService} injection
	 * </p>
	 */
	@Inject
	private ReservationService service;

	/**
	 * <p>
	 * A bean mapper for map {@link Reservation} and {@link ReservationBean}
	 */
	private ReservationBeanMapper mapperToBean = new ReservationBeanMapper();

	/**
	 * <p>
	 * An entity mapper for map {@link ReservationBean} and {@link Reservation}
	 */
	private ReservationDtoMapper mapperToDto = new ReservationDtoMapper();

	/**
	 * 
	 * <p>
	 * Method for retrieve all reservations without any search criteria.
	 * </p>
	 * 
	 * @return a list of all reservations
	 */
	@GET
	@Produces("application/json")
	public List<ReservationBean> retriveAllReservation() {
		try {
			ReservationSearchParams params = new ReservationSearchParams();
			return mapperToBean.toBeanList(service.retrieveReservation(params));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * <p>
	 * Method for retrieve reservation by ID.
	 * </p>
	 * 
	 * @return a reservation by ID
	 */
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	public List<ReservationBean> retriveReservationById(@PathParam("id") Long id) {
		try {
			ReservationSearchParams params = new ReservationSearchParams();
			params.setId(id);
			return mapperToBean.toBeanList(service.retrieveReservation(params));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * <p>
	 * Method for retrieve reservation by ID.
	 * </p>
	 * 
	 * @return a reservation by ID
	 */
	@GET
	@Path("/meetingroom/flag/{id:[0-9][0-9]*}")
	@Produces("application/json")
	public Boolean isMeetingroomEmptyFlag(@PathParam("id") Long id) {
		try {
			return service.isMeetingroomEmpty(id);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * <p>
	 * Method for retrieve reservation by employee ID.
	 * </p>
	 * 
	 * @return a reservation by employee ID
	 */
	@GET
	@Path("/employee/{id:[0-9][0-9]*}")
	@Produces("application/json")
	public List<ReservationBean> retrieveREservationByEmployeeId(@PathParam("id") String id) {
		try {
			ReservationSearchParams params = new ReservationSearchParams();
			params.setReservedBy(id);
			return mapperToBean.toBeanList(service.retrieveReservationByEmployeeId(params));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * <p>
	 * Method for retrieve reservation by ID.
	 * </p>
	 * 
	 * @return a reservation by ID
	 */
	@GET
	@Path("/meetingroom/{id:[0-9][0-9]*}")
	@Produces("application/json")
	public List<ReservationBean> retrieveReservationsByMeetingroomId(@PathParam("id") Long meetingroomId) {
		try {
			ReservationSearchParams params = new ReservationSearchParams();
			params.setMeetingroomId(meetingroomId);
			return mapperToBean.toBeanList(service.retrieveReservation(params));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * <p>
	 * Method for delete reservation by their id.
	 * </p>
	 * 
	 * @param id
	 */
	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public void deleteReservationById(@PathParam("id") Long id) {
		try {
			service.deleteReservationById(id);
		} catch (Exception e) {

		}
	}

	/**
	 * 
	 * <p>
	 * Method for create new reservation.
	 * </p>
	 * 
	 * @param bean {@link ReservationBean}
	 * @throws Exception
	 */
	@POST
	@Consumes("application/json")
	public Response createNewReservation(ReservationBean bean) throws Exception {
		try {
			service.createNewReservation(mapperToDto.toDto(bean));
		} catch (Exception e) {

		}
		return Response
				.created(UriBuilder.fromResource(ResrvationEndpoint.class).path(String.valueOf(bean.getId())).build()).build();
	};

	@POST
	@Path("/time")
	@Produces("application/json")
	public List<ReservationBean> retrieveReservationsByStartTime(ReservationSearchParams params) throws Exception {
		try {
		return mapperToBean.toBeanList(service.retrieveReservationsByStartTime(params.getStartDate()));
		} catch (Exception e) { 
			return null; 
		}
	}

	/**
	 * 
	 * <p>
	 * Method for create new reservation and check if room is free.
	 * </p>
	 * 
	 * @param bean {@link ReservationBean}
	 * @throws Exception
	 */
	@POST
	@Path("/create")
	@Consumes("application/json")
	public Response createNewReservationAndCheckAvailability(ReservationBean bean) throws Exception {
		try {
			service.createNewReservationAndCheckAvailability(mapperToDto.toDto(bean));
			return Response.created(
					UriBuilder.fromResource(ResrvationEndpoint.class).path(String.valueOf(bean.getId())).build())
					.build();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * <p>
	 * Method for update reservation by their id.
	 * </p>
	 * 
	 * @param id
	 * @param bean {@link ReservationBean}
	 */
	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes("application/json")
	public void updateReservationById(@PathParam("id") Long id, ReservationBean bean) {
		try {
			service.updateReservationById(id, mapperToDto.toDto(bean));
		} catch (Exception e) {

		}
	}
	
}
