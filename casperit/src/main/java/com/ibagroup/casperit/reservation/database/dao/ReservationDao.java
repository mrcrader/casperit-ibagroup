package com.ibagroup.casperit.reservation.database.dao;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.ibagroup.casperit.reservation.database.dto.Reservation;

/**
 * 
 * <p>A DAO implementation for reservation</p>
 * 
 * @author IBA Group
 * @since 2019
 *
 */

//	Use @RequestScoped
//	https://stackoverflow.com/questions/27149388/no-bean-is-eligible-for-injection-to-the-injection-point

/*
 * We suppress the warning about not specifying a serialVersionUID, as we are still developing this app, and want the JVM to
 * generate the serialVersionUID for us. When we put this app into production, we'll generate and embed the serialVersionUID
 */
@SuppressWarnings("serial")
@RequestScoped
public class ReservationDao implements Serializable{

	/**
	 * <p>Injection of EntityMenager. unitName="primary" is the name of out persistence.xml</p>
	 */
	@PersistenceContext(unitName = "primary")
	private EntityManager em;
	
	@Inject
    private Event<Reservation> reservationEventSrc;
	
	/**
	 * <p> Return all reservations if other parameters are null </p>
	 * @param search parameters
	 * @return list of reservations
	 */
	public List<Reservation> retrieveReservationByParams(ReservationSearchParams params) throws NonUniqueResultException {
		if (params.getId() != null || params.getStartTime() != null || params.getEndTime() != null || params.getReservationDescription() != null || params.getMeetingroomId() != null) {
			TypedQuery<Reservation> query = em.createQuery("SELECT r FROM Reservation r WHERE r.id = :id OR r.startTime = :startTime OR r.endTime = :endTime OR r.reservationDescription = :reservationDescription OR r.meetingroom.id = :meetingroomId", Reservation.class);
				if (params.getId() != null)
					query.setParameter("id", params.getId());
				else query.setParameter("id", null);
				if (params.getStartTime() != null)
					query.setParameter("startTime", params.getStartTime());
				else query.setParameter("startTime", null);
				if (params.getEndTime() != null)
					query.setParameter("endTime", params.getEndTime());
				else query.setParameter("endTime", null);
				if (params.getReservationDescription() != null)
					query.setParameter("reservationDescription", params.getReservationDescription());
				else query.setParameter("reservationDescription", null);
				if (params.getMeetingroomId() != null)
					query.setParameter("meetingroomId", params.getMeetingroomId());
				else query.setParameter("meetingroomId", null);
				try {
					return (List<Reservation>) query.getResultList();
				} catch (NoResultException exc) {
					return null;
				}
		} else {
			TypedQuery<Reservation> query = em.createQuery("SELECT DISTINCT r FROM Reservation r ORDER BY r.id", Reservation.class);
			try {
				return (List<Reservation>) query.getResultList();
			} catch (NoResultException exc) {
				return null;
			}
		}
	}
	
	public List<Reservation> retrieveReservationByMeetingroomIdAndTimeParams(ReservationSearchParams params) throws NonUniqueResultException {
		if (params.getCurrentTime() != null || params.getCurrentTimeMinusTenMinutes() != null || params.getCurrentTimePlusTenMinutes() != null || params.getMeetingroomId() != null ) {
			TypedQuery<Reservation> query = em.createQuery("SELECT r FROM Reservation r WHERE r.meetingroom.id = :meetingroomId AND ((r.startTime > :currentTimeMinusTenMinutes OR r.startTime > :currentTimePlusTenMinutes) AND r.endTime <= :currentTime)", Reservation.class);
				if (params.getCurrentTimeMinusTenMinutes() != null)
					query.setParameter("currentTimeMinusTenMinutes", params.getCurrentTimeMinusTenMinutes());
				else query.setParameter("currentTimeMinusTenMinutes", null);
				if (params.getCurrentTimePlusTenMinutes() != null)
					query.setParameter("currentTimePlusTenMinutes", params.getCurrentTimePlusTenMinutes());
				else query.setParameter("currentTimePlusTenMinutes", null);
				if (params.getCurrentTime() != null)
					query.setParameter("currentTime", params.getCurrentTime());
				else query.setParameter("currentTime", null);
				if (params.getMeetingroomId() != null)
					query.setParameter("meetingroomId", params.getMeetingroomId());
				else query.setParameter("meetingroomId", null);
				try {
					return (List<Reservation>) query.getResultList();
				} catch (NoResultException exc) {
					return null;
				}
		} else {
			return null;
		}
	}
	
	public List<Reservation> checkMeetingRoomAvailabilityByTimeParams(Long startTimeFromUser, Long endTimeFromUser) throws NonUniqueResultException {       
		TypedQuery<Reservation> query = em.createQuery("SELECT r FROM Reservation r WHERE r.startTime <= :startTimeFromUser AND r.endTime >= :endTimeFromUser AND r.endTime >= :startTimeFromUser ORDER BY r.id", Reservation.class);
		query.setParameter("startTimeFromUser", startTimeFromUser);
		query.setParameter("endTimeFromUser", endTimeFromUser);
		try {
			return (List<Reservation>) query.getResultList();
		} catch (NoResultException exc) {
			return null;
		}
	}
	
	public List<Reservation> retrieveReservationsByMeetingroomId(Long meetingroomId) throws NonUniqueResultException {       
		TypedQuery<Reservation> query = em.createQuery("SELECT r FROM Reservation r WHERE r.meetingroom.id = :meemeetingroomId ORDER BY r.id", Reservation.class);
		query.setParameter("meetingroomId", meetingroomId);
		try {
			return (List<Reservation>) query.getResultList();
		} catch (NoResultException exc) {
			return null;
		}
	}
	
	public List<Reservation> retrieveReservationsByStartTime(Long startDate) throws NonUniqueResultException {       
		TypedQuery<Reservation> query = em.createQuery("SELECT r FROM Reservation r WHERE r.startDate = :startDate ORDER BY r.id", Reservation.class);
		query.setParameter("startDate", startDate);
		try {
			return (List<Reservation>) query.getResultList();
		} catch (NoResultException exc) {
			return null;
		}
	}
	
	public List<Reservation> retrieveReservationByEmployeeId(ReservationSearchParams params) throws NonUniqueResultException {       
		TypedQuery<Reservation> query = em.createQuery("SELECT r FROM Reservation r WHERE r.reservedBy.id = :employeeId ORDER BY r.id", Reservation.class);
		query.setParameter("employeeId", params.getReservedBy());
		try {
			return (List<Reservation>) query.getResultList();
		} catch (NoResultException exc) {
			return null;
		}
	}

//	Some code example	
//
//	public Order findOrderSubmittedAt(Date date) throws NonUniqueResultException {
//		Query q = entityManager.createQuery(
//			"SELECT e FROM " + entityClass.getName() + " e WHERE date = :date_at");
//		q.setParameter("date_at", date);
//		try {
//			return (Order) q.getSingleResult();
//		} catch (NoResultException exc) {
//			return null;
//		}
//	}
//	 
//	public Order getOrderSubmittedAt(Date date) throws NoResultException, NonUniqueResultException {
//		Query q = entityManager.createQuery(
//			"SELECT e FROM " + entityClass.getName() + " e WHERE date = :date_at");
//		q.setParameter("date_at", date);
//		return (Order) q.getSingleResult();
//	}
//	
//	
// https://xebia.com/blog/jpa-implementation-patterns-data-access-objects/#JpaDao
//
//	public class JpaOrderDao extends JpaDao<Integer, Order> implements OrderDao {
//		public List<Order> findOrdersSubmittedSince(Date date) {
//			Query q = entityManager.createQuery(
//				"SELECT e FROM " + entityClass.getName() + " e WHERE date >= :date_since");
//			q.setParameter("date_since", date);
//			return (List<Order>) q.getResultList();
//		}
//	}
//

    public void createNewReservation(Reservation dto) throws Exception {
    	try {
	        em.persist(dto);
	        reservationEventSrc.fire(dto);
    	} catch (Exception e) {
    	
		}
    }

    public void deleteReservationById(Long id) {
    	Reservation dto = em.find(Reservation.class, id);
	      if (dto == null) {
	    	  dto = new Reservation();
	      }
	      em.remove(dto);
	   }
    
	public void updateReservationById(Long id, Reservation dtoToUpdate) {
	      TypedQuery<Reservation> findByIdQuery = em.createQuery("SELECT DISTINCT r FROM Reservation r WHERE r.id = :entityId ORDER BY r.id", Reservation.class);
	      findByIdQuery.setParameter("entityId", id);
	      Reservation dto;
	      try {
	         dto = findByIdQuery.getSingleResult();
	      }
	      catch (NoResultException nre) {
	         dto = null;
	      }
	      dto.setStartTime(dtoToUpdate.getStartTime());
	      dto.setEndTime(dtoToUpdate.getEndTime());
	      em.merge(dto);
	   }
	
}
