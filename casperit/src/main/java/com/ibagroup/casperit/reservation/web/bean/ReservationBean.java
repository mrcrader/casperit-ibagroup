package com.ibagroup.casperit.reservation.web.bean;

import com.ibagroup.casperit.meetingroom.web.bean.MeetingroomBean;

public class ReservationBean {

	private Long id;
	
	private String reservationDescription;
	
	private String phone;
	
	private Long startTime;

	private Long startDate;

	private Long endTime;

	private Long endDate;
	
	private String reservedBy;
	
	private MeetingroomBean meetingroom;
	
	private String optionalDescription;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReservationDescription() {
		return reservationDescription;
	}

	public void setReservationDescription(String reservationDescription) {
		this.reservationDescription = reservationDescription;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public String getReservedBy() {
		return reservedBy;
	}

	public void setReservedBy(String reservedBy) {
		this.reservedBy = reservedBy;
	}

	public MeetingroomBean getMeetingroom() {
		return meetingroom;
	}

	public void setMeetingroom(MeetingroomBean meetingroom) {
		this.meetingroom = meetingroom;
	}

	public String getOptionalDescription() {
		return optionalDescription;
	}

	public void setOptionalDescription(String optionalDescription) {
		this.optionalDescription = optionalDescription;
	}
	
}
