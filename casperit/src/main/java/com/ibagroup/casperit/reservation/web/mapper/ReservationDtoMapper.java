package com.ibagroup.casperit.reservation.web.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ibagroup.casperit.meetingroom.web.mapper.MeetingroomDtoMapper;
import com.ibagroup.casperit.reservation.database.dto.Reservation;
import com.ibagroup.casperit.reservation.web.bean.ReservationBean;

public class ReservationDtoMapper {

	private MeetingroomDtoMapper meetingroomMapper = new MeetingroomDtoMapper();
	
	public Reservation toDto(ReservationBean bean) {
		Reservation dto = null;
		if (bean != null) {
			dto = new Reservation();
			dto.setId(bean.getId());
			dto.setReservationDescription(bean.getReservationDescription());
			dto.setEndTime(bean.getEndTime());
			dto.setEndDate(bean.getEndDate());
			dto.setOptionalDescription(bean.getOptionalDescription());
			dto.setPhone(bean.getPhone());
			dto.setStartTime(bean.getStartTime());
			dto.setStartDate(bean.getStartDate());
			dto.setMeetingroom(meetingroomMapper.toDto(bean.getMeetingroom()));
			dto.setReservedBy(bean.getReservedBy());
		}
		return dto;
		
	}
	
	public List<Reservation> toDtoList(List<ReservationBean> reservationBeans){
		
		final List<Reservation> reservations = new ArrayList<Reservation>();
		if(reservationBeans != null) {
			for(ReservationBean reservationBean : reservationBeans) {
				Reservation dto = new Reservation();
				dto.setId(reservationBean.getId());
				dto.setReservationDescription(reservationBean.getReservationDescription());
				dto.setEndTime(reservationBean.getEndTime());
				dto.setEndDate(reservationBean.getEndDate());
				dto.setOptionalDescription(reservationBean.getOptionalDescription());
				dto.setPhone(reservationBean.getPhone());
				dto.setStartTime(reservationBean.getStartTime());
				dto.setStartDate(reservationBean.getStartDate());
				dto.setMeetingroom(meetingroomMapper.toDto(reservationBean.getMeetingroom()));
				dto.setReservedBy(reservationBean.getReservedBy());
				reservations.add(dto);
			}
		}
		return reservations;
		
	}
	
}
