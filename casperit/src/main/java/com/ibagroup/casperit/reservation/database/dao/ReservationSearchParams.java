package com.ibagroup.casperit.reservation.database.dao;

import java.util.Date;

public class ReservationSearchParams {

	private Long id;
	
	private String reservationDescription;
	
	private String phone;
	
	private Long startTime;
	
	private Long startDate;
	
	private Long endTime;
	
	private Long endDate;
	
	private String reservedBy;
	
	private String meetingroomCommonNameResourceName;
	
	private String meetingroomResourceDescription;
	
	private String meetingroomSite;
	
	private String optionalDescription;
	
	private Long meetingroomId;
	
	private Date currentTimeMinusTenMinutes;
	
	private Date currentTimePlusTenMinutes;
	
	private Date currentTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReservationDescription() {
		return reservationDescription;
	}

	public void setReservationDescription(String reservationDescription) {
		this.reservationDescription = reservationDescription;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public String getReservedBy() {
		return reservedBy;
	}

	public void setReservedBy(String reservedBy) {
		this.reservedBy = reservedBy;
	}

	public String getMeetingroomCommonNameResourceName() {
		return meetingroomCommonNameResourceName;
	}

	public void setMeetingroomCommonNameResourceName(String meetingroomCommonNameResourceName) {
		this.meetingroomCommonNameResourceName = meetingroomCommonNameResourceName;
	}

	public String getMeetingroomResourceDescription() {
		return meetingroomResourceDescription;
	}

	public void setMeetingroomResourceDescription(String meetingroomResourceDescription) {
		this.meetingroomResourceDescription = meetingroomResourceDescription;
	}

	public String getMeetingroomSite() {
		return meetingroomSite;
	}

	public void setMeetingroomSite(String meetingroomSite) {
		this.meetingroomSite = meetingroomSite;
	}

	public String getOptionalDescription() {
		return optionalDescription;
	}

	public void setOptionalDescription(String optionalDescription) {
		this.optionalDescription = optionalDescription;
	}

	public Long getMeetingroomId() {
		return meetingroomId;
	}

	public void setMeetingroomId(Long meetingroomId) {
		this.meetingroomId = meetingroomId;
	}

	public Date getCurrentTimeMinusTenMinutes() {
		return currentTimeMinusTenMinutes;
	}

	public void setCurrentTimeMinusTenMinutes(Date currentTimeMinusTenMinutes) {
		this.currentTimeMinusTenMinutes = currentTimeMinusTenMinutes;
	}

	public Date getCurrentTimePlusTenMinutes() {
		return currentTimePlusTenMinutes;
	}

	public void setCurrentTimePlusTenMinutes(Date currentTimePlusTenMinutes) {
		this.currentTimePlusTenMinutes = currentTimePlusTenMinutes;
	}

	public Date getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(Date currentTime) {
		this.currentTime = currentTime;
	}
	
}
