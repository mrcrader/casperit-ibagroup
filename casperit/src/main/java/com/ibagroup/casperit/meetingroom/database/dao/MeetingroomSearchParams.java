package com.ibagroup.casperit.meetingroom.database.dao;

import java.util.List;

public class MeetingroomSearchParams {

	private Long id;
	private String commonNameResourceName;
	private String site;
	private String resourceDescription;
	private String capacity;
	private String resourceType;
	private String autoProcessType;
	private List<String> autoProcessForwardTo;
	private List<String> alertTo;
	
	public Long getId() {
		return id;
	}
	
	public String getCommonNameResourceName() {
		return commonNameResourceName;
	}
	
	public String getSite() {
		return site;
	}
	
	public String getResourceDescription() {
		return resourceDescription;
	}
	
	public String getCapacity() {
		return capacity;
	}
	
	public String getResourceType() {
		return resourceType;
	}
	
	public String getAutoProcessType() {
		return autoProcessType;
	}
	
	public List<String> getAutoProcessForwardTo() {
		return autoProcessForwardTo;
	}
	
	public List<String> getAlertTo() {
		return alertTo;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setCommonNameResourceName(String commonNameResourceName) {
		this.commonNameResourceName = commonNameResourceName;
	}
	
	public void setSite(String site) {
		this.site = site;
	}
	
	public void setResourceDescription(String resourceDescription) {
		this.resourceDescription = resourceDescription;
	}
	
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	
	public void setAutoProcessType(String autoProcessType) {
		this.autoProcessType = autoProcessType;
	}
	
	public void setAutoProcessForwardTo(List<String> autoProcessForwardTo) {
		this.autoProcessForwardTo = autoProcessForwardTo;
	}
	
	public void setAlertTo(List<String> alertTo) {
		this.alertTo = alertTo;
	}
	
}
