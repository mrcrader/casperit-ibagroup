package com.ibagroup.casperit.meetingroom.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.ibagroup.casperit.meetingroom.database.dao.MeetingroomDao;
import com.ibagroup.casperit.meetingroom.database.dao.MeetingroomSearchParams;
import com.ibagroup.casperit.meetingroom.database.dto.Meetingroom;

/**
 * 
 * @author IBA Group
 * @since 2019
 * 
 * A service for {@link Meetingroom}
 *
 */

@Stateless
public class MeetingroomService {
	
	@Inject
	private MeetingroomDao dao;
	
	/*
	 * Method for invoke all meetingrooms by params
	 */
	public List<Meetingroom> retrieveMeetingroom(MeetingroomSearchParams params){
		try {
			return dao.retrieveMeetingroomByParams(params);
		} catch (Exception e) {
			return null;
		}
	}
    
    public void deleteMeetingroomById(Long id) {
    	try {
    		dao.deleteMeetingroomById(id);
    	} catch (Exception e) {
    		
		}
    }
    
    public void createNewMeetingroom(Meetingroom dto) throws Exception {
    	try {
    		dao.createNewMeetingroom(dto);
    	} catch (Exception e) {
  
		}
    
    }
    
    public void updateMeetingroomById(Long id, Meetingroom dto) {
    	try {
    		dao.updateMeetingroomById(id, dto);
    	} catch (Exception e) {

		}
    }

}
