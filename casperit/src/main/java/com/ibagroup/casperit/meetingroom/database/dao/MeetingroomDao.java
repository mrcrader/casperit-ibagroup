package com.ibagroup.casperit.meetingroom.database.dao;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.ibagroup.casperit.meetingroom.database.dto.Meetingroom;

/* <p>A DAO implementation for meetingrooms</p>
* 
* @author IBA Group
* @since 2019
*
*/

//	Use @RequestScoped
//	https://stackoverflow.com/questions/27149388/no-bean-is-eligible-for-injection-to-the-injection-point

/*
* We suppress the warning about not specifying a serialVersionUID, as we are still developing this app, and want the JVM to
* generate the serialVersionUID for us. When we put this app into production, we'll generate and embed the serialVersionUID
*/
@SuppressWarnings("serial")
@RequestScoped
public class MeetingroomDao implements Serializable{

	/**
	 * <p>Injection of EntityMenager. unitName="primary" is the name of out persistence.xml</p>
	 */
	@PersistenceContext(unitName = "primary")
	private EntityManager em;
	
	@Inject
   private Event<Meetingroom> meetingroomEventSrc;
	
	/**
	 * <p> Return all reservations if other parameters are null </p>
	 * @param search parameters
	 * @return list of reservations
	 */
	public List<Meetingroom> retrieveMeetingroomByParams(MeetingroomSearchParams params) throws NonUniqueResultException {
		if (params.getId() != null || params.getCapacity() != null || params.getResourceDescription() != null || params.getSite() != null || params.getCommonNameResourceName() != null ) {
			TypedQuery<Meetingroom> query = em.createQuery("SELECT m FROM Meetingroom m WHERE m.id = :id OR m.capacity = :capacity OR m.resourceDescription = :resourceDescription OR m.site = :site OR m.commonNameResourceName = :commonNameResourceName", Meetingroom.class);
				if (params.getId() != null)
					query.setParameter("id", params.getId());
				else query.setParameter("id", null);
				if (params.getCapacity() != null)
					query.setParameter("capacity", params.getCapacity());
				else query.setParameter("capacity", null);
				if (params.getResourceDescription() != null)
					query.setParameter("resourceDescription", params.getResourceDescription());
				else query.setParameter("resourceDescription", null);
				if (params.getSite() != null)
					query.setParameter("site", params.getSite());
				else query.setParameter("site", null);
				if (params.getCommonNameResourceName() != null)
					query.setParameter("commonNameResourceName", params.getCommonNameResourceName());
				else query.setParameter("commonNameResourceName", null);
				try {
					return (List<Meetingroom>) query.getResultList();
				} catch (NoResultException exc) {
					return null;
				}
		} else {
			TypedQuery<Meetingroom> query = em.createQuery("SELECT DISTINCT m FROM Meetingroom m ORDER BY m.id", Meetingroom.class);
			try {
				return (List<Meetingroom>) query.getResultList();
			} catch (NoResultException exc) {
				return null;
			}
		}
	}

//	Some code example	
//
//	public Order findOrderSubmittedAt(Date date) throws NonUniqueResultException {
//		Query q = entityManager.createQuery(
//			"SELECT e FROM " + entityClass.getName() + " e WHERE date = :date_at");
//		q.setParameter("date_at", date);
//		try {
//			return (Order) q.getSingleResult();
//		} catch (NoResultException exc) {
//			return null;
//		}
//	}
//	 
//	public Order getOrderSubmittedAt(Date date) throws NoResultException, NonUniqueResultException {
//		Query q = entityManager.createQuery(
//			"SELECT e FROM " + entityClass.getName() + " e WHERE date = :date_at");
//		q.setParameter("date_at", date);
//		return (Order) q.getSingleResult();
//	}
//	
//	
//https://xebia.com/blog/jpa-implementation-patterns-data-access-objects/#JpaDao
//
//	public class JpaOrderDao extends JpaDao<Integer, Order> implements OrderDao {
//		public List<Order> findOrdersSubmittedSince(Date date) {
//			Query q = entityManager.createQuery(
//				"SELECT e FROM " + entityClass.getName() + " e WHERE date >= :date_since");
//			q.setParameter("date_since", date);
//			return (List<Order>) q.getResultList();
//		}
//	}
//

	public void createNewMeetingroom(Meetingroom dto) throws Exception {
   		try {
	        em.persist(dto);
	        meetingroomEventSrc.fire(dto);
   		} catch (Exception e) {
   	
		}
	}

   	public void deleteMeetingroomById(Long id) {
	   Meetingroom dto = em.find(Meetingroom.class, id);
	      if (dto == null) {
	    	  dto = new Meetingroom();
	      }
	      em.remove(dto);
	   }
   
	public void updateMeetingroomById(Long id, Meetingroom dtoToUpdate) {
	      TypedQuery<Meetingroom> findByIdQuery = em.createQuery("SELECT DISTINCT m FROM Meetingroom m WHERE m.id = :entityId ORDER BY m.id", Meetingroom.class);
	      findByIdQuery.setParameter("entityId", id);
	      Meetingroom dto;
	      try {
	         dto = findByIdQuery.getSingleResult();
	      }
	      catch (NoResultException nre) {
	         dto = null;
	      }
	      dto.setResourceDescription(dtoToUpdate.getResourceDescription());
	      em.merge(dto);
	   }
	
}
