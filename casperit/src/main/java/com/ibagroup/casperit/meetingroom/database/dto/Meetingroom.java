package com.ibagroup.casperit.meetingroom.database.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ibagroup.casperit.reservation.database.dto.Reservation;

/**
 * 
 * @author IBA Group
 * @since 2019
 *
 */

@SuppressWarnings("serial")
@Entity
@Table(name="Meetingroom")
public class Meetingroom implements Serializable{

	/**
	 * <p> ID </p>
	 */
	@Id
    @GeneratedValue
    private Long id;

	/**
	 * <p> Resource name. Consist of street name and room number. </p>
	 */
	@Column(name = "CommonNameResourceName")
	private String commonNameResourceName;
	
	/**
	 * <p> Resource address. Content only street name. </p>
	 */
	@Column(name = "Site")
	private String site;
	
	/**
	 * <p> Resource description may content additional data about meeting room like equipment and etc. </p>
	 */
	@Column(name = "ResourceDescription")
	private String resourceDescription;
	
	/**
	 * <p> Resource capacity in persons. </p>
	 */
	@Column(name = "Capacity")
	private String capacity;
	
	/**
	 * <p> ResourceType can be: 1. Room, 2. Other, 3. Online-meeting. </p>
	 */
    @Column(name = "ResourceType")
	private String resourceType;
	
	/**
	 * <p> AutoProcessType (owner restrictions) can be: 0.None, 1. Only owner, 2. Choose employees, 3. Auto processing, D. Cancel orders. </p>
	 */
	@Column(name = "AutoProcessType")
	private String autoProcessType;
	
	/**
	 * <p> Resource owner name. </p>
	 */
	private Long autoProcessForwardTo;
	
	/**
	 * <p> Alert about reservation goes to one (or even more) employee. </p>
	 */
	private Long alertTo;
	
	public Long getId() {
		return id;
	}

	public String getCommonNameResourceName() {
		return commonNameResourceName;
	}

	public String getSite() {
		return site;
	}

	public String getResourceDescription() {
		return resourceDescription;
	}

	public String getCapacity() {
		return capacity;
	}

	public String getResourceType() {
		return resourceType;
	}

	public String getAutoProcessType() {
		return autoProcessType;
	}

	public Long getAutoProcessForwardTo() {
		return autoProcessForwardTo;
	}

	public Long getAlertTo() {
		return alertTo;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public void setCommonNameResourceName(String commonNameResourceName) {
		this.commonNameResourceName = commonNameResourceName;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public void setResourceDescription(String resourceDescription) {
		this.resourceDescription = resourceDescription;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public void setAutoProcessType(String autoProcessType) {
		this.autoProcessType = autoProcessType;
	}

	public void setAutoProcessForwardTo(Long autoProcessForwardTo) {
		this.autoProcessForwardTo = autoProcessForwardTo;
	}

	public void setAlertTo(Long alertTo) {
		this.alertTo = alertTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alertTo == null) ? 0 : alertTo.hashCode());
		result = prime * result + ((autoProcessForwardTo == null) ? 0 : autoProcessForwardTo.hashCode());
		result = prime * result + ((autoProcessType == null) ? 0 : autoProcessType.hashCode());
		result = prime * result + ((capacity == null) ? 0 : capacity.hashCode());
		result = prime * result + ((commonNameResourceName == null) ? 0 : commonNameResourceName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((resourceDescription == null) ? 0 : resourceDescription.hashCode());
		result = prime * result + ((resourceType == null) ? 0 : resourceType.hashCode());
		result = prime * result + ((site == null) ? 0 : site.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Meetingroom other = (Meetingroom) obj;
		if (alertTo == null) {
			if (other.alertTo != null)
				return false;
		} else if (!alertTo.equals(other.alertTo))
			return false;
		if (autoProcessForwardTo == null) {
			if (other.autoProcessForwardTo != null)
				return false;
		} else if (!autoProcessForwardTo.equals(other.autoProcessForwardTo))
			return false;
		if (autoProcessType == null) {
			if (other.autoProcessType != null)
				return false;
		} else if (!autoProcessType.equals(other.autoProcessType))
			return false;
		if (capacity == null) {
			if (other.capacity != null)
				return false;
		} else if (!capacity.equals(other.capacity))
			return false;
		if (commonNameResourceName == null) {
			if (other.commonNameResourceName != null)
				return false;
		} else if (!commonNameResourceName.equals(other.commonNameResourceName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (resourceDescription == null) {
			if (other.resourceDescription != null)
				return false;
		} else if (!resourceDescription.equals(other.resourceDescription))
			return false;
		if (resourceType == null) {
			if (other.resourceType != null)
				return false;
		} else if (!resourceType.equals(other.resourceType))
			return false;
		if (site == null) {
			if (other.site != null)
				return false;
		} else if (!site.equals(other.site))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Meetingroom [id=" + id + ", commonNameResourceName=" + commonNameResourceName + ", site=" + site
				+ ", resourceDescription=" + resourceDescription + ", capacity=" + capacity + ", resourceType="
				+ resourceType + ", autoProcessType=" + autoProcessType + ", autoProcessForwardTo="
				+ autoProcessForwardTo + ", alertTo=" + alertTo + ", isEmptyFlag=" + "]";
	}
	
}
