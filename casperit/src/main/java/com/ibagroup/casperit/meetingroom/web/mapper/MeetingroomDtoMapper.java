package com.ibagroup.casperit.meetingroom.web.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ibagroup.casperit.meetingroom.database.dto.Meetingroom;
import com.ibagroup.casperit.meetingroom.web.bean.MeetingroomBean;

public class MeetingroomDtoMapper {
	
	public Meetingroom toDto(MeetingroomBean bean) {
		Meetingroom dto = null;
		if (bean != null) {
			dto = new Meetingroom();
			dto.setAlertTo(bean.getAlertTo());
			dto.setAutoProcessForwardTo(bean.getAutoProcessForwardTo());
			dto.setAutoProcessType(bean.getAutoProcessType());
			dto.setCapacity(bean.getCapacity());
			dto.setCommonNameResourceName(bean.getCommonNameResourceName());
			dto.setId(bean.getId());
			dto.setResourceDescription(bean.getResourceDescription());
			dto.setResourceType(bean.getResourceType());
			dto.setSite(bean.getSite());
		}
		return dto;
		
	}
	
	public List<Meetingroom> toDtoList(List<MeetingroomBean> meetingroomBeans){
		
		final List<Meetingroom> meetingrooms = new ArrayList<Meetingroom>();
		if(meetingroomBeans != null) {
			for(MeetingroomBean meetingroomBean : meetingroomBeans) {
				Meetingroom dto = new Meetingroom();
				dto.setAlertTo(meetingroomBean.getAlertTo());
				dto.setAutoProcessForwardTo(meetingroomBean.getAutoProcessForwardTo());
				dto.setAutoProcessType(meetingroomBean.getAutoProcessType());
				dto.setCapacity(meetingroomBean.getCapacity());
				dto.setCommonNameResourceName(meetingroomBean.getCommonNameResourceName());
				dto.setId(meetingroomBean.getId());
				dto.setResourceDescription(meetingroomBean.getResourceDescription());
				dto.setResourceType(meetingroomBean.getResourceType());
				dto.setSite(meetingroomBean.getSite());
				meetingrooms.add(dto);
			}
		}
		return meetingrooms;
		
	}
	
}
