package com.ibagroup.casperit.meetingroom.web.rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.ibagroup.casperit.meetingroom.database.dao.MeetingroomSearchParams;
import com.ibagroup.casperit.meetingroom.database.dto.Meetingroom;
import com.ibagroup.casperit.meetingroom.service.MeetingroomService;
import com.ibagroup.casperit.meetingroom.web.bean.MeetingroomBean;
import com.ibagroup.casperit.meetingroom.web.mapper.MeetingroomBeanMapper;
import com.ibagroup.casperit.meetingroom.web.mapper.MeetingroomDtoMapper;

/**
 * 
 * @author IBA Group
 * @since 2019
 *
 */

@Path("meetingrooms")
@Stateless
public class MeetingroomEndpoint {

	/**
	 * <p>{@link MeetingroomService} injection</p>
	 */
	@Inject
	private MeetingroomService service;
	
	/**
	 * <p>A bean mapper for map {@link Meetingroom} and {@link MeetingroomBean}
	 */
	private MeetingroomBeanMapper mapperToBean = new MeetingroomBeanMapper();
	
	/**
	 * <p>An entity mapper for map {@link MeetingroomBean} and {@link Meetingroom}
	 */
	private MeetingroomDtoMapper mapperToDto = new MeetingroomDtoMapper();
  
	/**
	 * 
	 * <p>Method for retrieve all meeting rooms without any search criteria.</p>
	 * 
	 * @return a list of all meeting rooms
	 */
	@GET
	@Produces("application/json")
	public List<MeetingroomBean> retriveAllMeetingrooms() {
		try {
			MeetingroomSearchParams params = new MeetingroomSearchParams();
	  		return mapperToBean.toBeanList(service.retrieveMeetingroom(params));
	  	} catch (Exception e) {
				return null;
		}
	}
	  
	/**
	  * 
	  * <p>Method for retrieve meeting room by ID.</p>
	  * 
	  * @return a meeting room by ID
	  */
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	public List<MeetingroomBean> retriveMeetingroomById(@PathParam("id") Long id) {
		try {
			MeetingroomSearchParams params = new MeetingroomSearchParams();
	  		params.setId(id);
	  		return mapperToBean.toBeanList(service.retrieveMeetingroom(params));
	  	} catch (Exception e) {
				return null;
		}
	}
	
	/**
	  * <p>Method for retrieve meeting room by address(site).</p>
	  * 
	  * @param bean {@link MeetingroomBean}
	  * @throws Exception
	  */
	@POST
	@Path("/address")
	@Produces("application/json")
	public List<MeetingroomBean> retrieveMeetingroomByAddress(MeetingroomSearchParams params) throws Exception {
		try {
	  		return mapperToBean.toBeanList(service.retrieveMeetingroom(params));
	    	} catch (Exception e) {
	    		return null;
			}
	}
  
	/**
	  * 
	  * <p>Method for delete meeting room by their id.</p>
	  * 
	  * @param id
	  */
	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public void deleteMeetingroomById(@PathParam("id") Long id) {
		try {
			service.deleteMeetingroomById(id);
	    } catch (Exception e) {
	   
		}
	}
	    
	/**
	  * 
	  * <p>Method for create new meeting room.</p>
	  * 
	  * @param bean {@link MeetingroomBean}
	  * @throws Exception
	  */
	@POST
	@Consumes("application/json")
	public void createNewMeetinroom(MeetingroomBean bean) throws Exception {
		try {
	    		service.createNewMeetingroom(mapperToDto.toDto(bean));
	    	} catch (Exception e) {
	    		
			}
	}
		
	/**
	  * 
	  * <p>Method for update meeting room by their id.</p>
	  * 
	  * @param id
	  * @param bean {@link MeetingroomBean}
	  */
	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes("application/json")
	public void updateMeetingroomById(@PathParam("id") Long id, MeetingroomBean bean) {
		try {
	    	service.updateMeetingroomById(id, mapperToDto.toDto(bean));
	    	} catch (Exception e) {
	    		
			}
	}
	
}
