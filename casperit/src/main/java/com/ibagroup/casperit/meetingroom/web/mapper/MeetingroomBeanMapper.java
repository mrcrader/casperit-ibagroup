package com.ibagroup.casperit.meetingroom.web.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ibagroup.casperit.meetingroom.database.dto.Meetingroom;
import com.ibagroup.casperit.meetingroom.web.bean.MeetingroomBean;

public class MeetingroomBeanMapper {
	
	public MeetingroomBean toBean(Meetingroom dto) {
		MeetingroomBean bean = null;
		if(dto != null) {
			bean = new MeetingroomBean();
			bean.setId(dto.getId());
			bean.setAlertTo(dto.getAlertTo());
			bean.setAutoProcessForwardTo(dto.getAutoProcessForwardTo());
			bean.setAutoProcessType(dto.getAutoProcessType());
			bean.setCapacity(dto.getCapacity());
			bean.setCommonNameResourceName(dto.getCommonNameResourceName());
			bean.setResourceDescription(dto.getResourceDescription());
			bean.setResourceType(dto.getResourceType());
			bean.setSite(dto.getSite());
		}
		return bean;
	}
	
	public List<MeetingroomBean> toBeanList(List<Meetingroom> meetingroomEntities){
		final List<MeetingroomBean> meetingroomBeans = new ArrayList<MeetingroomBean>();
		if(meetingroomEntities != null) {
			for(Meetingroom meetingroomEntity : meetingroomEntities) {
				MeetingroomBean bean = new MeetingroomBean();
				bean.setId(meetingroomEntity.getId());
				bean.setAlertTo(meetingroomEntity.getAlertTo());
				bean.setAutoProcessForwardTo(meetingroomEntity.getAutoProcessForwardTo());
				bean.setAutoProcessType(meetingroomEntity.getAutoProcessType());
				bean.setCapacity(meetingroomEntity.getCapacity());
				bean.setCommonNameResourceName(meetingroomEntity.getCommonNameResourceName());
				bean.setResourceDescription(meetingroomEntity.getResourceDescription());
				bean.setResourceType(meetingroomEntity.getResourceType());
				bean.setSite(meetingroomEntity.getSite());
				meetingroomBeans.add(bean);
			}
		}
		return meetingroomBeans;
	}
	
}
