package com.ibagroup.casperit.email;

import java.util.Properties;

import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.ibagroup.casperit.reservation.web.rest.ResrvationEndpoint;

@Path("email")
@Stateless
public class EmailEndpoint {
	
	public static String from = "tibo2013w@gmail.com";
	
	public static String password = "NKnnshfhr78";
	
	public static String sub = "CasperIT - Meeting!";

	  public void sendEmail(final String from,final String password,String to,String sub,String msg){   
	  Properties props = new Properties();    
	  props.put("mail.smtp.host", "smtp.gmail.com");    
	  props.put("mail.smtp.socketFactory.port", "465");    
	  props.put("mail.smtp.socketFactory.class",    
	            "javax.net.ssl.SSLSocketFactory");    
	  props.put("mail.smtp.auth", "true");    
	  props.put("mail.smtp.port", "465");    
	  Session session = Session.getDefaultInstance(props,    
	   new javax.mail.Authenticator() {    
	   protected PasswordAuthentication getPasswordAuthentication() {    
	   return new PasswordAuthentication(from,password);  
	   }    
	  });    
	  try {    
	       MimeMessage message = new MimeMessage(session);    
	       message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));    
	       message.setSubject(sub);    
	       message.setText(msg);    
	       //send message  
	       Transport.send(message);    
	       System.out.println("message sent successfully");    
	  } catch (MessagingException e) {throw new RuntimeException(e);
	  
	  }    
	     
	}
	  
	@POST
	@Path("/send")
	@Consumes("application/json")
	public Response createNewReservationAndCheckAvailability(Email bean) throws Exception {
		String msg = 	"Hello! "
				+ "\n\nYou have been invited for meeting!  \n\nRoom: " + bean.getRoom() + "\nLead:  " + 
				bean.getFrom() + "\nTime:  " + 
				bean.getDate() + 
				"\n\n With best wishes,\n Team CasperIT.";
		try {
	    		sendEmail(from, password, bean.getTo(), sub, msg);
	    		return Response.created(UriBuilder.fromResource(ResrvationEndpoint.class).path(String.valueOf(bean.getRoom())).build()).build();
	    	} catch (Exception e) {
	    		return null;
			}
	}   
	
}
