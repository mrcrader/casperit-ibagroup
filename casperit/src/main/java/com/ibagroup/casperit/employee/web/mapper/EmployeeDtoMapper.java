package com.ibagroup.casperit.employee.web.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ibagroup.casperit.employee.database.dto.Employee;
import com.ibagroup.casperit.employee.web.bean.EmployeeBean;

public class EmployeeDtoMapper {
	
	public Employee toDto(EmployeeBean bean) {
		Employee dto = null;
		if (bean != null) {
			dto = new Employee();
			dto.setEmail(bean.getEmail());
			dto.setId(bean.getId());
			dto.setName(bean.getName());
			dto.setPhone(bean.getPhone());
			dto.setPosition(bean.getPosition());
			dto.setSurname(bean.getSurname());
		}
		return dto;	
	}
	
	public List<Employee> toDtoList(List<EmployeeBean> employeeBeans){
		final List<Employee> employees = new ArrayList<Employee>();
		if(employeeBeans != null) {
			for(EmployeeBean employeeBean : employeeBeans) {
				Employee dto = new Employee();
				dto.setEmail(employeeBean.getEmail());
				dto.setId(employeeBean.getId());
				dto.setName(employeeBean.getName());
				dto.setPhone(employeeBean.getPhone());
				dto.setPosition(employeeBean.getPosition());
				dto.setSurname(employeeBean.getSurname());
				employees.add(dto);
			}
		}
		return employees;
	}
	
}
