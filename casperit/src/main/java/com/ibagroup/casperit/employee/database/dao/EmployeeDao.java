package com.ibagroup.casperit.employee.database.dao;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.ibagroup.casperit.employee.database.dto.Employee;

/**
 * 
 * <p>A DAO implementation for employees</p>
 * 
 * @author IBA Group
 * @since 2019
 *
 */

//	Use @RequestScoped
//	https://stackoverflow.com/questions/27149388/no-bean-is-eligible-for-injection-to-the-injection-point

/*
 * We suppress the warning about not specifying a serialVersionUID, as we are still developing this app, and want the JVM to
 * generate the serialVersionUID for us. When we put this app into production, we'll generate and embed the serialVersionUID
 */
@SuppressWarnings("serial")
@RequestScoped
public class EmployeeDao implements Serializable{

	/**
	 * <p>Injection of EntityMenager. unitName="primary" is the name of out persistence.xml</p>
	 */
	@PersistenceContext(unitName = "primary")
	private EntityManager em;
	
	@Inject
    private Event<Employee> employeeEventSrc;
	
	/**
	 * <p> Return all employees if other parameters are null </p>
	 * @param search parameters
	 * @return list of employees
	 */
	public List<Employee> retrieveEmployeeByParams(EmployeeSearchParams params) throws NonUniqueResultException {
		if (params.getId() != null || params.getName() != null || params.getEmail() != null || params.getSurname() != null || params.getPosition() != null || params.getPhone() != null) {
			TypedQuery<Employee> query = em.createQuery("SELECT e FROM Employee e WHERE e.id = :id OR e.name = :name OR e.email = :email OR e.surname = :surname OR e.position = :position OR e.phone = :phone", Employee.class);
				if (params.getId() != null)
					query.setParameter("id", params.getId());
				else query.setParameter("id", null);
				if (params.getName() != null)
					query.setParameter("name", params.getName());
				else query.setParameter("name", null);
				if (params.getEmail() != null)
					query.setParameter("email", params.getEmail());
				else query.setParameter("email", null);
				if (params.getSurname() != null)
					query.setParameter("surname", params.getSurname());
				else query.setParameter("surname", null);
				if (params.getPosition() != null)
					query.setParameter("position", params.getPosition());
				else query.setParameter("position", null);
				if (params.getPhone() != null)
					query.setParameter("phone", params.getPhone());
				else query.setParameter("phone", null);
				try {
					return (List<Employee>) query.getResultList();
				} catch (NoResultException exc) {
					return null;
				}
		} else {
			TypedQuery<Employee> query = em.createQuery("SELECT DISTINCT e FROM Employee e ORDER BY e.id", Employee.class);
			try {
				return (List<Employee>) query.getResultList();
			} catch (NoResultException exc) {
				return null;
			}
		}
	}

//	Some code example	
//
//	public Order findOrderSubmittedAt(Date date) throws NonUniqueResultException {
//		Query q = entityManager.createQuery(
//			"SELECT e FROM " + entityClass.getName() + " e WHERE date = :date_at");
//		q.setParameter("date_at", date);
//		try {
//			return (Order) q.getSingleResult();
//		} catch (NoResultException exc) {
//			return null;
//		}
//	}
//	 
//	public Order getOrderSubmittedAt(Date date) throws NoResultException, NonUniqueResultException {
//		Query q = entityManager.createQuery(
//			"SELECT e FROM " + entityClass.getName() + " e WHERE date = :date_at");
//		q.setParameter("date_at", date);
//		return (Order) q.getSingleResult();
//	}
//	
//	
// https://xebia.com/blog/jpa-implementation-patterns-data-access-objects/#JpaDao
//
//	public class JpaOrderDao extends JpaDao<Integer, Order> implements OrderDao {
//		public List<Order> findOrdersSubmittedSince(Date date) {
//			Query q = entityManager.createQuery(
//				"SELECT e FROM " + entityClass.getName() + " e WHERE date >= :date_since");
//			q.setParameter("date_since", date);
//			return (List<Order>) q.getResultList();
//		}
//	}
//

    public void createNewEmployee(Employee dto) throws Exception {
    	try {
	        em.persist(dto);
	        employeeEventSrc.fire(dto);
    	} catch (Exception e) {
    	
		}
    }

    public void deleteEmployeeById(Long id) {
    	Employee dto = em.find(Employee.class, id);
	      if (dto == null) {
	    	  dto = new Employee();
	      }
	      em.remove(dto);
	   }
	
}

