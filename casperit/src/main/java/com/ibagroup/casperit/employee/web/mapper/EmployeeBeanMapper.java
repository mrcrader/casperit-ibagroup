package com.ibagroup.casperit.employee.web.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ibagroup.casperit.employee.database.dto.Employee;
import com.ibagroup.casperit.employee.web.bean.EmployeeBean;

public class EmployeeBeanMapper {

	public EmployeeBean toBean(Employee entity) {
		EmployeeBean bean = null;
		if(entity != null) {
			bean = new EmployeeBean();
			bean.setId(entity.getId());
			bean.setEmail(entity.getEmail());
			bean.setName(entity.getName());
			bean.setPhone(entity.getPhone());
			bean.setPosition(entity.getPosition());
			bean.setSurname(entity.getSurname());
		}
		return bean;
	}
	
	public List<EmployeeBean> toBeanList(List<Employee> employeeEntities){
		
		final List<EmployeeBean> employeeBeans = new ArrayList<EmployeeBean>();
		if(employeeEntities != null) {
			for(Employee employeeEntity : employeeEntities) {
				EmployeeBean bean = new EmployeeBean();
				bean.setId(employeeEntity.getId());
				bean.setEmail(employeeEntity.getEmail());
				bean.setName(employeeEntity.getName());
				bean.setPhone(employeeEntity.getPhone());
				bean.setPosition(employeeEntity.getPosition());
				bean.setSurname(employeeEntity.getSurname());
				employeeBeans.add(bean);
			}
		}
		return employeeBeans;
	}
	
}
