package com.ibagroup.casperit.employee.web.bean;

public class EmployeeBean {

	private Long id;
	
	private String name;
	
	private String surname;
	
	private String phone;
	
	private String position;
	
	private String email;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getPhone() {
		return phone;
	}

	public String getPosition() {
		return position;
	}

	public String getEmail() {
		return email;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
