package com.ibagroup.casperit.employee.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.ibagroup.casperit.employee.database.dao.EmployeeDao;
import com.ibagroup.casperit.employee.database.dao.EmployeeSearchParams;
import com.ibagroup.casperit.employee.database.dto.Employee;

/**
 * 
 * @author IBA Group
 * @since 2019
 * 
 * A service for {@link Employee}
 *
 */
@Stateless
public class EmployeeService {
	
	@Inject
	private EmployeeDao dao;
	
	/*
	 * Method for invoke all coffee by params
	 */
	public List<Employee> retrieveEmployee(EmployeeSearchParams params){
		try {
			return dao.retrieveEmployeeByParams(params);
		} catch (Exception e) {
			return null;
		}
	}
    
    public void deleteEmployeeById(Long id) {
    	try {
    		dao.deleteEmployeeById(id);
    	} catch (Exception e) {
    		
		}
    }
    
    public void createNewEmployee(Employee dto) throws Exception {
    	try {
    		dao.createNewEmployee(dto);
    	} catch (Exception e) {
  
		}
    
    }

}
