# casperit-ibagroup

## Explore Rest APIs

The app defines following CRUD APIs.
    
    For reservation:

    GET http://172.20.3.170:8080/casperit/rest/reservation/

    POST 172.20.3.170:8080/casperit/rest/reservation/

    GET 172.20.3.170:8080/casperit/rest/reservation/{id}

    PUT 172.20.3.170:8080/casperit/rest/reservation/{id}

    DELETE 172.20.3.170:8080/casperit/rest/reservation/{id}
    
    GET 172.20.3.170:8080/casperit/rest/reservation/create

    For Meeting Rooms

    POST 172.20.3.170:8080/casperit/rest/meetingrooms

    GET 172.20.3.170:8080/casperit/rest/meetingrooms

    GET 172.20.3.170:8080/casperit/rest/meetingrooms{id}

    PUT 172.20.3.170:8080/casperit/rest/meetingrooms

    DELETE 172.20.3.170:8080/casperit/rest/meetingrooms{id}


You can test them using Postman or any other rest client.
